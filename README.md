# Capacitor 3 & OneSignal basic App

Basic starter project for Capacitor 3 and OneSignal (SDK 3 and OneSignal Plugin version 3)


## Requirements

For requirements you can follow the Capacitor 3 and OneSignal Guides:

- [Capacitor 3 environment setup](https://capacitorjs.com/docs/getting-started/environment-setup)
- [Capacitor 3 Installation guide](https://capacitorjs.com/docs/getting-started)
- [OneSignal Ionic & Capacitor SDK Setup](https://documentation.onesignal.com/docs/ionic-sdk-setup)


## Installation

- Run `npm install` in the main project folder.
- Follow the usage instructions below.


## Usage

1) Update the current `config.sample.js` file with **your OneSignal AppID** and save as `config.js` in the `js` folder.

2) Run `npx cap sync` command for sync assets between project and native enviroments

3) Run the app from xCode or launch it with `npx cap run ios` CLI command.
