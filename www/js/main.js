import { ONESIGNAL_APPID } from './config.js';

let subscriptionState = 'pending...';
let DOMsubscriptionState = document.querySelector('.js-onesignal-subscription');

function launchApp(data) {
    DOMsubscriptionState.innerText = subscriptionState;
    console.log('>>> launchApp data: ' + JSON.stringify(data));
}

function initOneSignal() {
    // Doc ref: https://documentation.onesignal.com/docs/ionic-sdk-setup
    // Cordova API Reference: https://documentation.onesignal.com/docs/cordova-30-api-reference
    // Enable to debug issues.
    // window.plugins.OneSignal.setLogLevel(4, 4);
    // window.plugins.OneSignal.setLogLevel(6, 0);

    window.plugins.OneSignal.setAppId(ONESIGNAL_APPID);
    window.plugins.OneSignal.setNotificationOpenedHandler(function(jsonData) {
        console.log('>>> notificationOpenedHandler: ' + JSON.stringify(jsonData));
    });

    window.plugins.OneSignal.promptForPushNotificationsWithUserResponse(function(accepted) {
        console.log(">>> User accepted notifications: " + accepted);
        subscriptionState = accepted;
        DOMsubscriptionState.innerText = subscriptionState;
    });

    window.plugins.OneSignal.getDeviceState(function (data) {
        console.log('>>> getDeviceState callback');
        launchApp(data);
    });
};

function initApp() {
    console.log('>>> initApp, OneSignal APPID: ' + ONESIGNAL_APPID);
    DOMsubscriptionState.innerText = subscriptionState;

    document.addEventListener('deviceready', function () {
        initOneSignal();
    });
};


initApp();
